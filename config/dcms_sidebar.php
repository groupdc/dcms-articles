<?php

return [
    'Articles' => [
        'icon' => 'fa-pencil',
        'links' => [
            ['route' => 'admin/articles', 'label' => 'Articles', 'permission' => 'articles-browse'],
            ['route' => 'admin/articles/categories', 'label' => 'Categories', 'permission' => 'articles-browse'],
        ],
    ],
];
