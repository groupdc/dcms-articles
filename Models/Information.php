<?php
namespace Dcms\Dcmsarticles\Models;

use Dcms\Core\Models\EloquentDefaults;

class Information extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'products_information';
    protected $fillable = ['language_id', 'title', 'description'];
}
