<?php
namespace Dcms\Dcmsarticles\Models;

use Dcms\Core\Models\EloquentDefaults;

class Detail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'articles_language';
    protected $fillable = ['language_id', 'article_id', 'title', 'text', 'description', 'script'];

    public function article()
    {
        return $this->belongsTo('Dcms\Dcmsarticles\Models\Article', 'article_id', 'id');
    }

    public function articlecategory()
    {
        return $this->belongsTo('Dcms\Dcmsarticles\Models\Category', 'article_category_id', 'id');
    }

    public function pages()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcms\Pages\Models\Pageslanguage', 'articles_language_to_pages', 'article_detail_id', 'page_id');
    }

    //		public function products()
//		{
//			// BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
//			return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Information', 'articles_language_to_products_information', 'articles_language_id', 'products_information_id');
//		}
}
