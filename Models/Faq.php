<?php
namespace Dcms\Dcmsarticles\Models;

use Dcms\Core\Models\EloquentDefaults;

class Faq extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'articles_faq';
    protected $fillable = ['language_id', 'article_id', 'question', 'answer'];

    public function article()
    {
        return $this->belongsTo('Dcms\Dcmsarticles\Models\Article', 'article_id', 'id');
    }
}
