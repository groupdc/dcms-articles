<?php
namespace Dcms\Dcmsarticles\Models;

use Dcms\Core\Models\EloquentDefaults;

class Article extends EloquentDefaults
{
    protected $connection = 'project';

    public function detail()
    {
        return $this->hasMany('Dcms\Dcmsarticles\Models\Detail', 'article_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo("Dcms\Dcmsarticles\Models\Category", 'article_category_id', 'id');
        //return $this->hasOne("Category");
    }

    public function productinformation()
    {
        return $this->belongsToMany('Dcms\Dcmsarticles\Models\Information', 'articles_to_products_information_group', 'article_id', 'information_id')->withTimestamps();
    }

    public function article()
    {
        return $this->belongsToMany('Dcms\Dcmsarticles\Models\Article', 'articles_to_articles', 'article_id', 'taggedarticle_id')->withTimestamps();
    }

    public function plants()
    {
        return $this->belongsToMany('Dcms\Plants\Models\Plant', 'article_to_plant', 'article_id', 'plant_id')->withTimestamps();
    }
}
