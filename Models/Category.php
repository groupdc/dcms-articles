<?php
namespace Dcms\Dcmsarticles\Models;

//use Dcweb\Dcms\Models\EloquentDefaults;

use DB;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use NodeTrait;

    protected $connection = 'project';
    protected $table = 'articles_categories_language';
    protected $fillable = ['language_id', 'title'];

    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public static function OptionValueTreeArray($enableEmpty = false, $columns = ['*'], $columnMapper = ['id', 'title', 'language_id'])
    {
        $PageObj = DB::connection('project')
                            ->table('articles_categories_language as node')
                            ->select(
                                (DB::connection('project')->raw("CONCAT( REPEAT( '-', node.depth ), node.title) AS title")),
                                'node.id',
                                'node.parent_id',
                                'node.language_id',
                                'node.depth',
                                (DB::connection('project')->raw('Concat("<img src=\'/packages/Dcms/Core/assets/images/flag-",lcase(country),".svg\' style=\'width:16px; height: auto;\' >") as regio'))
                            )
                            ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
                            ->orderBy('node.lft')
                            ->get();

        $OptionValueArray = [];

        if (count($PageObj) > 0) {
            foreach ($PageObj as $lang) {
                if (array_key_exists($lang->language_id, $OptionValueArray) == false) {
                    $OptionValueArray[$lang->language_id] = [];
                }

                //we  make an array with array[languageid][maincategoryid] = translated category;
                $val = $columnMapper[1];
                $key = $columnMapper[0];
                $OptionValueArray[$lang->language_id][$lang->$key] = /*str_repeat('-',$lang->level).' '.*/$lang->$val;
            }
        } elseif ($enableEmpty === true) {
            $Languages = Language::all();
            foreach ($Languages as $Lang) {
                $OptionValueArray[$Lang->id][1] = '- ROOT -';
            }
        }
        return $OptionValueArray;
    }

    public static function updateUrlPath($old, $new)
    {
        DB::connection('project')->update('UPDATE articles_categories_language SET url_path = replace(url_path, "' . $old . '", "' . $new . '" ) WHERE url_path LIKE "' . $old . '%" ;  ');
        return true;
    }

    public function saveDepth()
    {
        $this->depth = self::withDepth()->find($this->id)->depth;
        $this->save();
    }
}
