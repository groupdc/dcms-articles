@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Articles</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li class="active"><i class="far fa-newspaper"></i> Articles</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">

                    @if (Session::has('message'))
                        <div class="alert alert-info">{!! Session::get('message') !!}</div>
                    @endif

                    @can('articles-add')
                        <div class="btnbar btnbar-right"><a class="btn btn-small btn-primary" href="{!! URL::to('/admin/articles/create') !!}">Create new</a></div>
                    @endcan

                    <h2>Overview</h2>
                    <div class="panel filter up">
                        <div class="panel-heading">
                            <h3>Filter <i class="far fa-plus-square right"></i></h3>
                        </div>
                        <div class="panel-body" @if(Session::has('pagefilter')) style="display:block;" @endif>
                            <form method="GET" action="/admin/articles" accept-charset="UTF-8">

                                <?php
                                if (Request::has('resetfilters') && Request::get('resetfilters') == 'true') {
                                    Session::forget('pagefilter');
                                }
                                ?>

                                @if(Request::has('page') || Session::has('pagefilter'))
                                    @if(Request::has('page'))
                                        <?php
                                        Session::put('pagefilter', Request::get('page')); //put in a session so the Datatable request can accesss the variables
                                        ?>
                                    @endif
                                @endif

                                <h4>Country</h4>
                                <ul>
                                    <li style="display:inline;"><label for="be"><input class="changeLanguage" type="checkbox" name="page[language_id][1]" id="be" value="1"
                                                                                       @if(Session::has('pagefilter.language_id') && Session::has('pagefilter.language_id.1')) checked @endif >
                                            <img src="/packages/Dcms/Core/images/flag-be.svg" style="width:16px; height: auto;"></label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="nl"><input class="changeLanguage" type="checkbox" name="page[language_id][3]"
                                                                                                         id="nl" value="3"
                                                                                                         @if(Session::has('pagefilter.language_id') && Session::has('pagefilter.language_id.3')) checked @endif>
                                            <img src="/packages/Dcms/Core/images/flag-nl.svg" style="width:16px; height: auto;"></label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="fr"><input class="changeLanguage" type="checkbox" name="page[language_id][6]"
                                                                                                         id="fr" value="6"
                                                                                                         @if(Session::has('pagefilter.language_id') && Session::has('pagefilter.language_id.6')) checked @endif>
                                            <img src="/packages/Dcms/Core/images/flag-fr.svg" style="width:16px; height: auto;"></label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="de"><input class="changeLanguage" type="checkbox" name="page[language_id][7]"
                                                                                                         id="de" value="7"
                                                                                                         @if(Session::has('pagefilter.language_id') && Session::has('pagefilter.language_id.7')) checked @endif>
                                            <img src="/packages/Dcms/Core/images/flag-de.svg" style="width:16px; height: auto;"></label></li>
                                </ul>
                                <hr>
                                <h4>Division</h4>
                                <ul>
                                    <li style="display:inline;"><label for="hobby"><input class="changeDivision" type="checkbox" name="page[division][hobby]" id="hobby"
                                                                                          value="hobby"
                                                                                          @if(Session::has('pagefilter.division') && Session::has('pagefilter.division.hobby')) checked @endif>
                                            Hobby</label></li>
                                    <li style="display:inline; margin-left:15px;"><label for="pro"><input class="changeDivision" type="checkbox" name="page[division][pro]"
                                                                                                          id="pro" value="pro"
                                                                                                          @if(Session::has('pagefilter.division') && Session::has('pagefilter.division.pro')) checked @endif>
                                            Professional</label></li>
                                </ul>
                                <hr>
                                <h4>Sector</h4>
                                <select class="form-control" name="page[sector][]">
                                    <option value="">-all-</option>
                                    <?php
                                    $allreadyprinted = [];
                                    ?>
                                    @foreach($sectors as $sector)
                                        <option class='sectordd sectorlanguage{{$sector->language_id}} sectordivision{{strtolower($sector->division)}}'
                                                value="{{$sector->title}}"
                                                @if(Session::has('pagefilter.sector') && Session::get('pagefilter.sector.0') == $sector->title ) selected @endif>{{$sector->title}}</option>
                                    @endforeach

                                </select>
                                <div class="btnbar btnbar-left" style="margin-top:15px;">
                                    {!! Form::button('Set Filter', array('class' => 'btn btn-primary', 'type'=>'submit')) !!}
                                    <a class="btn btn-default" href="{{url('admin/articles?resetfilters=true')}}">Reset Filter</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Country</th>
                            <th>Highpriority</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>

                    <script type="text/javascript">
                        $(document).ready(function () {

                            oTable = $('#datatable').DataTable({
                                "pageLength": 50,
                                "processing": true,
                                "serverSide": true,
                                "ajax": "{{ route('admin.articles.api.table') }}",
                                "columns": [
                                    {data: 'id', name: 'ID'},
                                    {data: 'title', name: 'articles_language.title'},
                                    {data: 'country', name: 'country', searchable: false},
                                    {data: 'highpriority', name: 'highpriority', searchable: false},
                                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                                ]
                            });
                        });
                    </script>

                    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

                </div>
            </div>
        </div>
    </div>
@stop

@section("script")
    <script type="text/javascript">

        languageFilter = "";
        divisionFilter = "";

        function filterLanguage() {
            $('.changeLanguage').each(function (index) {
                if ($(this)[0].checked == true) {
                    languageFilter = '.sectorlanguage' + $(this)[0].value;
                    $('.sectorlanguage' + $(this)[0].value + divisionFilter).show();
                } else {
                    $('.sectorlanguage' + $(this)[0].value).hide();
                }
            });
        }

        function filterDivision() {
            $('.changeDivision').each(function (index) {
                if ($(this)[0].checked == true) {
                    divisionFilter = '.sectordivision' + $(this)[0].value;
                    $(languageFilter + '.sectordivision' + $(this)[0].value).show();
                } else {
                    $('.sectordivision' + $(this)[0].value).hide();
                }
            });
        }

        $(document).ready(function () {
            $('.sectordd').hide();
            filterLanguage();
            filterDivision();

            $('.changeLanguage').change(function () {
                filterLanguage();
            })

            $('.changeDivision').change(function () {
                filterDivision();
            })

            //Filter toggle
            $('.filter .panel-heading').click(function () {
                $(this).closest('.panel').toggleClass('up');
            });

            $("#btn-reset").click(function () {
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);
                $(".condition").remove();
                $("#filters").prepend(appendDiv.attr('id', '0'));
                $("#0").find('select').val('0');
                $("#0").find('input[type=text]').val('');
            });

            //Add condition
            $('.add-condition').click(function () {
                var id = $(".condition:last").attr('id');
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);

                appendDiv.attr('id', ++id).insertAfter(".condition:last");

                $("#" + id).find('select').val('0');
                $("#" + id).find('input[type=text]').val('');

                return false;
            });

        });
    </script>
@stop
