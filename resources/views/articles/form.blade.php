@extends("dcms::template/layout")

@section("content")


    <div class="main-header">
        <h1>Articles</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/articles') !!}"><i class="far fa-newspaper"></i> Articles</a></li>
            @if(isset($article))
                <li class="active"><i class="far fa-pencil"></i> Article {{$article->id}}</li>
            @else
                <li class="active"><i class="far fa-plus-circle"></i> Create article</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        @if(isset($article))
            {!! Form::model($article, array('route' => array('admin.articles.update', $article->id), 'method' => 'PUT', 'onsubmit'=>'return resetTables();'  )) !!}
        @else
            {!! Form::open(array('url' => 'admin/articles', 'onsubmit'=>'return resetTables();'  )) !!}
        @endif

        <div class="row">
            <div class="col-md-9">
                <div class="main-content-tab tab-container">
                    @if (!is_array($categoryOptionValues) || count($categoryOptionValues)<=0 )    Please first create a <a
                            href="{!! URL::to('admin/articles/categories/create') !!}"> article category </a>  @else
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#information" role="tab" data-toggle="tab">Information</a></li>
                            <li><a href="#pages" role="tab" data-toggle="tab">Pages</a></li>
                            <li><a href="#articles" role="tab" data-toggle="tab">Articles</a></li>
                            <li><a href="#products" role="tab" data-toggle="tab">Products</a></li>
                            <li><a href="#plants" role="tab" data-toggle="tab">Plants</a></li>
                            <li><a href="#faq" role="tab" data-toggle="tab">FAQ</a></li>
                        </ul>

                        <div class="tab-content">

                            <div id="information" class="tab-pane active">
                                <!-- #information -->
                                @if($errors->any())
                                    <div class="alert alert-danger">{!! Html::ul($errors->all()) !!}</div>
                                @endif

                                @if(isset($languages))
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach($languages as $key => $language)
                                            <li class="{!! (( intval(session('overrule_default_by_language_id')) == intval($language->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '') ) !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab"
                                                                                               data-toggle="tab"><img
                                                            src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="16"
                                                            height="16"/> {!! $language->language_name !!}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        @foreach($languages as $key => $information)
                                            <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">
                                                {!! Form::hidden('article_information_id[' . $information->language_id . ']', $information->id) !!}

                                                <div class="form-group">
                                                    {!! Form::label('category_id[' . $information->language_id . ']', 'Category') !!}
                                                    {!! isset($categoryOptionValues[$information->language_id])? Form::select('category_id[' . $information->language_id . ']', $categoryOptionValues[$information->language_id], (old('category_id[' . $information->language_id . ']') ? old('category_id[' . $information->language_id . ']') : $information->article_category_id), array('class' => 'form-control')):'No categories found' !!}
                                                </div>
                                                    <!-- -->
                                                    <div class="form-group">
                                                        {!! Form::checkbox('highpriority['.$information->language_id.']', '1', ((isset($information->highpriority) && $information->highpriority==1)?1:0), array('class' => 'form-checkbox','id'=>'highpriority'. $information->language_id))  !!}
                                                        {!! Html::decode(Form::label('highpriority'. $information->language_id, 'High priority', array('class' => (isset($information->highpriority) && $information->highpriority==1)?'checkbox active':'checkbox'))) !!}
                                                    </div>
                                                    <!-- -->
                                                    <div class="form-group">
                                                        {!! Form::checkbox('megamenu['.$information->language_id.']', '1', ((isset($information->megamenu) && $information->megamenu==1)?1:0), array('class' => 'form-checkbox','id'=>'megamenu'. $information->language_id))  !!}
                                                        {!! Html::decode(Form::label('megamenu'. $information->language_id, 'Mega menu', array('class' => (isset($information->megamenu) && $information->megamenu==1)?'checkbox active':'checkbox'))) !!}
                                                    </div>
                                                    <!-- -->
                                                    <div class="form-group">
                                                        {!! Form::checkbox('largevisual['.$information->language_id.']', '1', ((isset($information->largevisual) && $information->largevisual==1)?1:0), array('class' => 'form-checkbox','id'=>'largevisual'. $information->language_id))  !!}
                                                        {!! Html::decode(Form::label('largevisual'. $information->language_id, 'Large visual', array('class' => (isset($information->largevisual) && $information->largevisual==1)?'checkbox active':'checkbox'))) !!}
                                                    </div>

                                                    <div class="form-group">
                                                        {!! Form::label('title[' . $information->language_id . ']', 'Title') !!}
                                                        {!! Form::text('title[' . $information->language_id . ']', (old('title[' . $information->language_id . ']') ? old('title[' . $information->language_id . ']') : $information->title ), array('class' => 'form-control')) !!}
                                                    </div>

                                                    <div class="form-group">
                                                        {!! Form::label('description[' . $information->language_id . ']', 'Description') !!}
                                                        {!! Form::textarea('description[' . $information->language_id . ']', (old('description[' . $information->language_id . ']') ? old('description[' . $information->language_id . ']') : $information->description ), array('class' => 'form-control ckeditor')) !!}
                                                    </div>

                                                    <div class="form-group">
                                                        {!! Form::label('meta_description[' . $information->language_id . ']', 'Meta-Description') !!}
                                                        {!! Form::text('meta_description[' . $information->language_id . ']', (old('meta_description[' . $information->language_id . ']') ? old('meta_description[' . $information->language_id . ']') : $information->meta_description ), array('class' => 'form-control')) !!}
                                                    </div>


                                                <div class="form-group">
                                                    {!! Form::label('thumbnail_alt[' . $information->language_id . ']', 'Thumbnail alternative text') !!}
                                                    {!! Form::text('thumbnail_alt[' . $information->language_id . ']', (old('thumbnail_alt[' . $information->language_id . ']') ? old('thumbnail_alt[' . $information->language_id . ']') : $information->thumbnail_alt ), array('class' => 'form-control')) !!}
                                                </div>

                                                
                                                <div class="form-group">
                                                    {!! Form::label('body[' . $information->language_id . ']', 'Body') !!}
                                                    {!! Form::textarea('body[' . $information->language_id . ']', (old('body[' . $information->language_id . ']') ? old('body[' . $information->language_id . ']') : $information->body ), array('class' => 'form-control ckeditor')) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('url', 'url') !!}
                                                    <div class="input-group">
                                                        {!! Form::text('url[' . $information->language_id . ']', (old('url[' . $information->language_id . ']') ? old('url[' . $information->language_id . ']') : $information->url ), array('class' => 'form-control', 'id'=>'url'.$information->language_id )) !!}
                                                        <span class="input-group-btn">{!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server-files', 'id'=>'browse_url'.$information->language_id )) !!}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                   <label> Keywords </label><br/>
                                                    <textarea name="keywords[{{$information->language_id}}]" rows="4" class="form-control">@if(!is_null($information->keywords)){!!($information->keywords)!!}@endif</textarea>
                                                </div>

                                                <div class="form-group">
                                                   <label> Log </label><br/>
                                                    <textarea name="log[{{$information->language_id}}]" rows="4" class="form-control"></textarea>
                                                </div>

                                                <a href="#" id="advanced">Advanced</a>
                                                <div class="advanced d-none" style="margin-top: 15px;">
                                                    <div class="form-group">
                                                        <label>Script</label>
                                                        <textarea name="script[{{$information->language_id}}]" class="form-control" rows="20">
                                                        @if(!is_null($information->script)){!!($information->script)!!}@endif
                                                        </textarea>
                                                    </div>
                                                
                                                    <div class="form-group">
                                                        <label>Loghistory</label>
                                                        @if(!is_null($information->log))
                                                            {!!nl2br($information->log)!!}
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                        @endforeach
                                    </div>

                            @endif
                            <!-- #information -->
                            </div>

                            <div id="pages" class="tab-pane">
                                <div class="tab-content">

                                    <?php
                                    // *Very simple* recursive rendering function
                                    function renderNode($node, $pageOptionValuesSelected = [])
                                    {
                                        echo '<li class="language-' . $node->language_id . ' depth-' . $node->depth . '  ">';
                                        echo '<span>';
                                        if ($node->depth == 0) {
                                            echo $node->title . '<i class="far fa-plus-square"></i>';
                                        } else {
                                            $checked = false;
                                            if (in_array($node->id, $pageOptionValuesSelected)) {
                                                $checked = true;
                                            } ?>
                                    {!! Form::checkbox("page_id[".$node->language_id."][".$node->id."]", $node->id, $checked, array('class' => 'form-checkbox','id'=>'page_id-'.$node->id))  !!}
                                    {!! Form::label('page_id-'.$node->id, $node->title, array('class' => ($checked == true?'active':'').' checkbox','id'=>'chkbxpage_id-'.$node->id)) !!}
                                    <?php
                                        }
                                        echo '</span>';

                                        if ($node->children()->count() > 0) {
                                            $active = '';
                                            if (session('overrule_default_by_language_id') == $node->language_id) {
                                                $active = 'active';
                                            }
                                            echo "\r\n" . '<ul class="' . ($node->depth == 0 ? 'division ' . $active : ($node->depth == 1 ? 'sector' : 'subsector')) . '">' . "\r\n";
                                            foreach ($node->children as $child) {
                                                renderNode($child, $pageOptionValuesSelected);
                                            }
                                            echo '</ul>' . "\r\n" . "\r\n";
                                        }
                                        echo '</li>' . "\r\n";
                                    }

                                    $roots = Dcms\Pages\Models\Pageslanguage::whereIsRoot()->get();

                                    echo '<ul class="country">';
                                    foreach ($roots as $root) {
                                        renderNode($root, $pageOptionValuesSelected);
                                    }
                                    echo '</ul>';
                                    ?>

                                </div>
                            </div>

<!-- Articles -->
    <div id="articles" class="tab-pane">
        <div class="form-group">

            <table id="datatable_article" class="table table-hover table-condensed" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Country</th>
                    <th>ID</th> 
                  </tr>
              </thead>
            </table>

        </div>
    </div>


<!-- Products -->
                            <div id="products" class="tab-pane">
                                <div class="form-group">
                                    <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Title</th>
                                                <th>Country</th>
                                                <th>Division</th>
                                                <th>ID</th>         
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>


<!-- Plants -->
                            <div id="plants" class="tab-pane">
                                <div class="form-group">
                                    <table id="plants-datatable" class="table table-hover table-condensed" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Plant</th>
                                            <th>Country</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>


<!-- FAQ -->
                            <div id="faq" class="tab-pane">
                               
                                @if(isset($languages))
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach($languages as $key => $language)
                                            <li class="{!! (( intval(session('overrule_default_by_language_id')) == intval($language->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '') ) !!}"><a href="{!! '#faq' . $language->language . '-' . $language->country !!}" role="tab"
                                                                                               data-toggle="tab"><img
                                                            src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="16"
                                                            height="16"/> {!! $language->language_name !!}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">


                                        @foreach($languages as $key => $information)

                                        
                                            <div id="faq{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">

                                            <table class="table table-bordered table-striped" id="faq-table-{{$information->language_id}}">

                                                <thead>
                                                    <tr>
                                                        <th>Question & Answer</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                            
                                                <tbody id="tbody-faq-table-{{$information->language_id}}">
                                                    @if(isset($faq))
                                                        @foreach(collect($faq)->where('language_id',$information->language_id) as $faqdetail)
                                                            @include('dcms::articles.partials.faqrow', ['faq' => $faqdetail])
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                                    
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <button class="btn btn-default pull-right add-faq-row" id="tablefinder-faq-table-{{$information->language_id}}" type="button"><i class="far fa-plus"></i></button>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                                   
                                            </div>
                                        @endforeach
                                    </div>

                            @endif
                            <!-- #information -->
                            </div>



                        </div>
                </div>
            </div>
            <div class="col-md-3">
                <!-- -->
                <div class="main-content-block">
                    <div class="form-group setdate">
                        {!! Form::checkbox('enabletime', '1', null, array('class' => 'form-checkbox','id'=>'enabletime'))  !!}
                        {!! Html::decode(Form::label('enabletime', 'Set Date', array('class' => (isset($article) && $article->enabletime==1)?'checkbox active':'checkbox'))) !!}
                    </div>
                    <!-- -->
                    <div class="form-group startdate active">
                        {!! Form::label('startdate', 'Date Start') !!}
                        <div class="input-group input-append date">
                            {!! Form::text('startdate', old('startdate'), array('id'=>'Startdate','class' => 'form-control', 'readonly', 'size' => '16')) !!}
                            <span class="input-group-addon btn btn-primary"><i class="glyphicon glyphicon-th"></i></span>
                        </div>
                    </div>
                    <!-- -->
                    <div class="form-group enddate active">
                        {!! Form::label('enddate', 'Date End') !!}
                        <div class="input-group input-append date">
                            {!! Form::text('enddate', old('enddate'), array('id'=>'Enddate','class' => 'form-control', 'readonly', 'size' => '16')) !!}
                            <span class="input-group-addon btn btn-primary"><i class="glyphicon glyphicon-th"></i></span>
                        </div>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        {!! Form::checkbox('birthdaycycle', '1', null, array('class' => 'form-checkbox','id'=>'birthdaycycle'))  !!}
                        {!! Html::decode(Form::label('birthdaycycle', 'Repeating', array('class' => (isset($article) && $article->birthdaycycle==1)?'checkbox active':'checkbox'))) !!}
                    </div>
                    <!-- -->
                    <div class="form-group">
                        {!! Form::checkbox('online', '1', null, array('class' => 'form-checkbox','id'=>'online'))  !!}
                        {!! HTML::decode(Form::label('online', 'Online', array('class' => (isset($article) && $article->online==1)?'checkbox active':'checkbox'))) !!}
                    </div>
                    <!-- -->
                    <div class="form-group">
                        {!! Form::checkbox('newarticle',1, null, array('class' => 'form-checkbox','id'=>'newarticle'))!!}
                        {!! Html::decode(Form::label('newarticle', "New Article", array('class' => (isset($article) && $article->newarticle==1)?'checkbox active':'checkbox'))) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::checkbox('calendar',1, null, array('class' => 'form-checkbox','id'=>'calendar'))!!}
                        {!! Html::decode(Form::label('calendar', "Calendar", array('class' => (isset($article) && $article->calendar==1)?'checkbox active':'checkbox'))) !!}
                    </div>

                    {{--  @if(!isset($articleFormTemplate)  || is_null($articleFormTemplate) )

                      @elseif(!is_null($articleFormTemplate))
                        @include($articleFormTemplate)
                        @yield('mainForm')
                      @endif --}}

                </div>
                <!-- -->
                <div class="main-content-block">
                    <div class="form-group">
                        {!! Form::label('thumbnail', 'Thumbnail') !!}
                        <div class="thumbnail">
                            <img src="{{(isset($article) ?  $article->thumbnail: '') }}">
                        </div>
                        <div class="input-group">
                            {!! Form::text('thumbnail', old('thumbnail'), array('class' => 'form-control')) !!}
                            <span class="input-group-btn">{!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server', 'id'=>'browse_thumbnail')) !!}</span>
                        </div>
                    </div>
                </div>
                
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    <button type="submit" name="SaveAndFormAgain" value="SaveAndFormAgain" class='btn btn-primary'>Save</button>
                    {!! Form::submit('Save and Close', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        
        <div id="removed-faq"></div>
        {!! Form::close() !!}
    </div>

@stop

@section("script")

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap-datetimepicker.min.js') !!}"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('/packages/Dcms/Core/css/bootstrap-datetimepicker.min.css') !!}">

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKFinder
            $(".browse-server").click(function () {
                BrowseServer('Images:/articles/', 'thumbnail');
            })

            $("body").on("click", ".browse-server-files", function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('Files:/', returnid);
            });

            //CKEditor
            $("textarea[id='description']").ckeditor();
            $("textarea[id='body']").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            })

            //Datepicker
            //ToggleDate();

            $(".setdate label").click(function () {
                //ToggleDate();
            });

            //pagetree
            $(".country span").click(function () {
                $(this).find('i').toggleClass('fa-minus-square');
                $(this).next().toggleClass('active');
            });

            //Advanced
            $("#advanced").click(function() {
                $('.advanced').toggleClass('d-none');
                return false;
            });

            $(".add-faq-row").click(function () {
                theid = $(this).attr("id").substring(12,999);
                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.articles.api.faqrow') }}",
                    data: {_token: '{{ csrf_token() }}',language_id:theid.substring((theid.length)-1) },
                    success: function (result) {
                        $("#"+theid+" tbody").append(result);
                        $("#"+theid+" tbody tr:last td textarea").ckeditor();
                    }
                });
            });

        $(document).on('click', '.remove-faq-row', function () {
            var id = $(this).data('id');
            $(this).closest('tr').remove();
            $('#removed-faq').append('<input type="hidden" name="removed_faq[]" value="' + id + '">');
        });

        });

        /*function ToggleDate() {
            if ($('.setdate label').hasClass('active')) {
                $('.startdate, .enddate').addClass('active');
            } else {
                $('.startdate, .enddate').removeClass('active');
            }
        }*/
        $(function () {
            $('#Startdate, #Enddate').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
            });
        });

    </script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript">
        productsTable = null;
        plantsTable = null;
        function resetTables(){
            productsTable.search('').draw();
            plantsTable.search('').draw();
            return true;
        }

        <?php $id_helper = (isset($article) ? $article->id : 0); ?>
        $(document).ready(function() {

                articlesTable = $('#datatable_article').DataTable({
                        "paging": false,
                        "processing": true,
                        "serverSide": false,
                        "ajax": "{{ route('admin.articles.api.relativearticles.table',array('article_id'=>$id_helper)) }}",
                        "columns": [
                            {data: 'radio', name: 'radio'},
                            {data: 'title', name: 'title'},
                            {data: 'language', name: 'language'},
                            {data: 'article_id', name: 'article_id'},
                        ],
                        "createdRow": function( row, data, cells ) {
                                if (~data['radio'].indexOf('checked')) { 
                                $(row).addClass('selected');
                                }
                            }
                });

                $('#datatable_article').on('click', 'tbody tr', function () {
                    if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                    } else {
                        var clicked = $(this).find('input:checkbox');
                        if ( clicked.is(':checked')) {
                            clicked.prop('checked', false);
                        } else {
                            clicked.prop('checked', true);
                        }
            
                        $(this).toggleClass('selected');
                    }
                });

            productsTable = $('#datatable').DataTable({
                "processing": true,
                "paging": false,
                "ajax": "{{ route('admin.articles.api.products.table',$id_helper) }}",
                "columns": [
                    {data: 'radio', name: 'radio'},
                    {data: 'title', name: 'title'},
                    {data: 'language', name: 'language'},
                    {data: 'catalogue', name: 'catalogue'},
                    {data: 'information_group_id', name: 'information_group_id'},
                ],
                "createdRow": function( row, data, cells ) {
                    if (~data['radio'].indexOf('checked')) {   
                    $(row).addClass('selected');
                    }
                }
            });

            plantsTable = $('#plants-datatable').DataTable({
                "paging": false,
                "processing": true,
                "serverSide": false,
                "ajax": "{{ route('admin.articles.api.plants.table', $id_helper) }}",
                "columns": [
                    {data: 'radio', name: 'radio', width: '5%'},
                    {data: 'common', name: 'common'},
                    {data: 'language', name: 'language'}
                ]
            });
            
            $('#datatable').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });
            
            $('#plants-datatable').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });
        });
    </script>
@stop

@section('style')
<style type="text/css">
.d-none {
    display: none;
}
</style>
@stop