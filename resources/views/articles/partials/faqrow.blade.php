@php
    $id = (isset($faq) && !is_null($faq)) ? $faq->faqid : uniqid('new_');
    $question = isset($faq) ? $faq->question : '';
    $answer = isset($faq) ? $faq->answer : '';

$request = request();
debug($request->all());
    if ($request->has('language_id')){
        $language_id = $request->get('language_id');
    } else {
    $language_id = isset($faq) ? $faq->language_id : '';
    }
@endphp

@php
if(empty($id)) $id = uniqid('new_');
@endphp

<tr>
    <td>
        <input type="hidden" name="faq[{{$id}}][language_id]" value="{{$language_id}}"/>
        <label>Question</label>
        <input type="text" class="form-control" name="faq[{{$id}}][question]" value="{{$question}}"/>
        <label>Answer</label>
        <textarea name="faq[{{$id}}][answer]" class="form-control ckeditor">{{$answer}}</textarea>
    </td>
    <td>
        <button class="btn btn-default pull-right remove-faq-row" data-id="{{$id}}" type="button"><i class="far fa-trash-alt"></i></button>
    </td>
</tr>
    