@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Categories</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/articles') !!}"><i class="far fa-newspaper"></i> Articles</a></li>
            <li class="active"><i class="far fa-tag"></i> Categories</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">

                    @if (Session::has('message'))
                        <div class="alert alert-info">{!! Session::get('message') !!}</div>
                    @endif

                    @can('article-add')
                        <div class="btnbar btnbar-right"><a class="btn btn-small btn-primary" href="{!! URL::to('/admin/articles/categories/create') !!}">Create new</a></div>
                    @endcan
                    <h2>Overview</h2>

                    <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Country</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            oTable = $('#datatable').DataTable({
                                "pageLength": 50,
                                "processing": true,
                                "serverSide": true,
                                "ajax": "{{ route('admin.articles.categories.api.table') }}",
                                "columns": [
                                    {data: 'category', name: 'category'},
                                    {data: 'regio', name: 'regio', searchable: false},
                                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                                ]
                            });
                        });
                    </script>


                    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

                </div>
            </div>
        </div>
    </div>

@stop
