<?php
namespace Dcms\Dcmsarticles\Http\Controllers;

use DB;
use Auth;
use Form;
use Mail;
use View;
use Input;
use Session;
use DateTime;
use Redirect;
use Validator;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Dcms\Plants\Models\Plant;
use App\Jobs\ProcessNewArticle;
use Dcms\Dcmsarticles\Models\Faq;
use Dcms\Products\Models\Product;
use App\Jobs\ProcessUpdateArticle;
use App\Http\Controllers\Controller;
use Dcms\Dcmsarticles\Models\Detail;
use Dcms\Dcmsarticles\Models\Article;
use Dcms\Dcmsarticles\Models\Category;
use Illuminate\Support\Facades\Schema;

class ArticleController extends Controller
{
    public $article_language_Columnnames = [];
    public $article_Columnnames = [];
    public $article_ColumnNamesDefaults = [];
    public $information_ColumnNamesDefaults = [];

    public $articlelanguageFormTemplate = '';
    public $articleFormTemplate = '';

    public $extendgeneralTemplate = '';

    public function __construct()
    {
        $this->middleware('permission:articles-browse')->only('index');
        $this->middleware('permission:articles-add')->only(['create', 'store']);
        $this->middleware('permission:articles-edit')->only(['edit', 'update']);
        $this->middleware('permission:articles-delete')->only('destroy');

        //note startdate and enddate are defaults, since these are always there, and have a specific value (not simple text, or bool)
        $this->article_Columnnames = [
            'thumbnail' => 'thumbnail',
            'enabletime' => 'enabletime',
            'birthdaycycle' => 'birthdaycycle',
            'online' => 'online',
            'newarticle' => 'newarticle',
            'calendar' => 'calendar',
        ];

        $this->article_language_Columnnames = [
            'article_category_id' => 'category_id',
            'highpriority' => 'highpriority',
            'megamenu' => 'megamenu',
            'largevisual' => 'largevisual',
            'title' => 'title',
            'description' => 'description',
            'meta_description' => 'meta_description',
            'body' => 'body',
            'slug' => 'title',
            'path' => 'title',
            'url' => 'url',
            'log' => 'log',
            'keywords' => 'keywords',
            'script' => 'script',
            'thumbnail_alt' => 'thumbnail_alt',
        ];

        $this->article_ColumnNamesDefaults = [
            'enabletime' => '0',
            'birthdaycycle' => '0',
            'enablemorebtn' => '0',
            'online' => '0',
            'actionarticle' => '0',
            'thumbnailindetail' => '0',
            'newarticle' => '0',
            'calendar' => '0'];

        $this->information_ColumnNamesDefaults = [
            'highpriority' => '0', 
            'megamenu' => '0', 
            'largevisual' => '0', ];

        $this->articlelanguageFormTemplate = null;
        $this->articleFormTemplate = null;
    }

    public static function getArticlesDetailByLanguage($language_id = null)
    {
        return Detail::where('language_id', '=', $language_id)->lists('title', 'id');
    }

    public static function getDropdownArticlesByLanguage($language_id = null)
    {
        $dropdownvalues = ArticleController::getArticlesDetailByLanguage($language_id);

        return Form::select('article[' . $language_id . '][]', $dropdownvalues, [3, 4, 5, 6], ['id' => 'articles-' . $language_id, 'multiple' => 'multiple']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sectors = DB::connection('project')
            ->table('vwpages_language')
            ->select(
                'title',
                'language_id',
                'division'
            )
            ->where('depth', '=', '2')
            ->orderBy('title')
            ->get();

        // load the view
        return View::make('dcms::articles/index')->with('sectors', $sectors);
    }

    public function getDatatable()
    {
        $query = DB::connection('project')
            ->table('articles')
            ->select(
                'articles.id',
                'articles_language.title',
                'articles_language.highpriority',
                'articles_language.id as article_language_id',
                (DB::connection('project')->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as country'))
            )
            ->join('articles_language', 'articles.id', '=', 'articles_language.article_id')
            ->leftJoin('languages', 'articles_language.language_id', '=', 'languages.id');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('articles_language.language_id', session('overrule_default_by_language_id'));
        }

        if (Session::has('pagefilter')) {
            $filter = Session::get('pagefilter');

            foreach ($filter as $column => $colvalues) {
                if ($column == 'language_id') {
                    foreach ($colvalues as $colvalue) {
                        $query->orWhere('articles_language.language_id', '=', $colvalue);
                    }
                }

                $query->where(function ($q) use ($column, $colvalues) {
                    if ($column == 'language_id') {
                        foreach ($colvalues as $colvalue) {
                            $q->orWhereRaw('articles.id IN (SELECT id FROM vwarticles_to_pages WHERE language_id = "' . $colvalue . '" )');
                        }
                    }
                    if ($column == 'division') {
                        foreach ($colvalues as $colvalue) {
                            $q->orWhereRaw('articles.id IN (SELECT id FROM vwarticles_to_pages WHERE division_url = "' . $colvalue . '" )');
                        }
                    }
                    if ($column == 'sector') {
                        foreach ($colvalues as $colvalue) {
                            if ($colvalue <> '0' && $colvalue <> '' && !is_null($colvalue)) {
                                $q->orWhereRaw('articles.id IN (SELECT id FROM vwarticles_to_pages WHERE sector_url = "' . $colvalue . '" )');
                            }
                        }
                    }
                });
            }
        }

        return Datatables::queryBuilder($query)
                        ->addColumn('edit', function ($model) {
                            return '<form method="POST" action="/admin/articles/' . $model->article_language_id . '" accept-charset="UTF-8" class="pull-right">
							<input name="_token" type="hidden" value="' . csrf_token() . '">
							<input name="_method" type="hidden" value="DELETE">
							<a class="btn btn-xs btn-default" href="/admin/articles/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>
							<a class="btn btn-xs btn-default" href="/admin/articles/' . $model->article_language_id . '/copy"><i class="far fa-copy"></i></a>
							<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>
						</form>';
                        })
                        ->rawColumns(['country', 'edit'])
                        ->make(true) ;
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getProductsDatatable($article_id = 0)
    {
        $queryA = DB::connection('project')
        ->table('products_information as x')
        ->select(
            (
                DB::connection('project')->raw('
                case when (select count(*) from articles_to_products_information_group where articles_to_products_information_group.information_group_id = x.information_group_id and article_id = "' . $article_id . '") > 0 then 1 else 0 end as checked,
                catalogue,
                title,
                languages.country,
                information_group_id')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        // ->whereRaw('x.id IN (SELECT product_information_id FROM products_to_products_information WHERE product_id IN ( SELECT id FROM products WHERE online = 1 ) )')
        ->whereNotNull('x.information_group_id')
        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from articles_to_products_information_group where articles_to_products_information_group.information_group_id = x.information_group_id and article_id = "' . $article_id . '") > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="information_group_id[]" value="' . $model->information_group_id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->information_group_id . '" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->country) . '.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio', 'language'])
                ->make(true);
    }

    public function getRelationDatatable($product_id = 0)
    {
        $information_group_id = 0;
        $ProductInformation = Product::with('information')->find($product_id);

        if (isset($ProductInformation->information) && !is_null($ProductInformation->information) && $ProductInformation->information()->count() > 0) {
            foreach ($ProductInformation->information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_group_id = $I->information_group_id;
                    break;
                }
            }
        }

        $queryA = DB::connection('project')
                                        ->table('articles_language as x')
                                        ->select(
                                            (
                                                DB::connection('project')->raw('
																	article_id,
                                                                    title,
                                                                    languages.country,
																	case when (select count(*) from articles_to_products_information_group where articles_to_products_information_group.article_id = x.article_id and information_group_id = "' . $information_group_id . '") > 0 then 1 else 0 end as checked
																')
                                            )
                                        )
                                        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
                                        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from articles_to_products_information_group where articles_to_products_information_group.article_id = x.article_id and information_group_id = "' . $information_group_id . '") > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                        ->addColumn('radio', function ($model) {
                            return '<input type="checkbox" name="article_id[]" value="' . $model->article_id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->article_id . '" > ';
                        })
                        ->addColumn('language', function ($model) {
                            return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->country) . '.svg" style="width:16px; height: auto;" alt="">';
                        })
                        ->rawColumns(['radio', 'language'])
                        ->make(true) ;
    }

    public function getRelativeArticlesDatatable($article_id = 0)
    {
        // create the table!
        // -------------------
        // CREATE TABLE `dcm_acc`.`articles_to_articles` (`article_id` int AUTO_INCREMENT,`taggedarticle_id` int,`created_at` timestamp,`updated_at` timestamp, PRIMARY KEY (article_id, taggedarticle_id));

        $queryA = DB::connection('project')
                                        ->table('articles_language as x')
                                        ->select(
                                            (
                                                DB::connection('project')->raw('
																	article_id,
                                                                    title,
                                                                    languages.country,
																	case when (select count(*) from articles_to_articles where taggedarticle_id = x.article_id and article_id = "' . $article_id . '") > 0 then 1 else 0 end as checked
																')
                                            )
                                        )
                                        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
                                        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from articles_to_articles where taggedarticle_id = x.article_id and article_id = "' . $article_id . '") > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                        ->addColumn('radio', function ($model) {
                            return '<input type="checkbox" name="taggedarticle_id[]" value="' . $model->article_id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->article_id . '" > ';
                        })
                        ->addColumn('language', function ($model) {
                            return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->country) . '.svg" style="width:16px; height: auto;" alt="">';
                        })
                        ->rawColumns(['radio', 'language'])
                        ->make(true) ;
    }

    public function getPlantsDatatable($article_id = null)
    {
        $queryA = DB::connection('project')
        ->table('plants_language as x')
        ->select(
            (
                DB::connection('project')->raw('
                case when (select count(*) from article_to_plant where article_to_plant.plant_id = x.plant_id and article_id = "' . $article_id . '") > 0 then 1 else 0 end as checked,
                languages.country,
                x.plant_id, 
                x.common')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from article_to_plant where article_to_plant.plant_id = x.plant_id and article_id = "' . $article_id . '") > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="plant_ids[]" value="' . $model->plant_id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->plant_id . '" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->country) . '.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio', 'language'])
                ->make(true);
    }

    public function getFaqRow()
    {
        return view('dcms::articles.partials.faqrow')->render();
    }

    public function getFaq($id = null)
    {
        if (is_null($id)) {
            return DB::connection('project')->table('languages')->select((DB::connection('project')->raw("'' as question, '' as answer, '' as faqid")), 'id as language_id', 'language', 'country', 'language_name')->get();
        } else {
            return DB::connection('project')->select('
													SELECT languages.id as language_id, language, country, language_name, articles_faq.id as faqid, question, answer
													FROM articles_faq
													LEFT JOIN languages on languages.id = articles_faq.language_id
													WHERE  languages.id is not null AND  article_id = ?
													/*UNION
													SELECT languages.id , language, country, language_name,\'\',  \'\' , \'\' 
                                                    FROM languages
													WHERE id NOT IN (SELECT language_id FROM articles_faq WHERE article_id = ?)*/
                                                    ORDER BY 1
													', [$id, $id]);
        }
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection('project')->table('languages')->select((DB::connection('project')->raw("'' as title, 0 as highpriority, 0 as megamenu,0 as largevisual,  NULL as sort_id, (select max(sort_id) from articles_language where language_id = languages.id) as maxsort, '' as description , '' as meta_description , '' as body, '' as url, '' as article_category_id, '' as log, '' as keywords,'' as script, '' as thumbnail_alt, '' as id")), 'id as language_id', 'language', 'country', 'language_name')->get();
        } else {
            return DB::connection('project')->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, article_category_id, articles_language.id, article_id, title, sort_id, (select max(sort_id) from articles_language as X  where X.language_id = articles_language.language_id) as maxsort,  description, body, url, date_format(startdate,\'%d-%m-%Y\') as startdate , date_format(enddate,\'%d-%m-%Y\')  as enddate, meta_description, log, keywords, script, thumbnail_alt, highpriority, megamenu, largevisual
													FROM articles_language
													LEFT JOIN languages on languages.id = articles_language.language_id
													LEFT JOIN articles on articles.id = articles_language.article_id
													WHERE  languages.id is not null AND  article_id = ?
													UNION
													SELECT languages.id , language, country, language_name, \'\' , \'\' ,  \'\' , \'\' , NULL as sort_id, (select max(sort_id) from articles_language where language_id = languages.id) as maxsort, \'\' ,\'\' , \'\'  , \'\' , \'\',\'\',\'\' ,\'\',\'\' ,\'\' , 0, 0, 0
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM articles_language WHERE article_id = ?) ORDER BY 1
													', [$id, $id]);
        }
    }

    public function getExtendedModel()
    {
        //do nothing let the extend class hook into this
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = $this->getInformation();

        // load the create form (app/views/articles/create.blade.php)
        return View::make('dcms::articles/form')
        ->with('languages', $languages)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('pageOptionValuesSelected', [])
            ->with('sortOptionValues', $this->getSortOptions($languages, 1))
            ->with('articlelanguageFormTemplate', $this->articlelanguageFormTemplate)
            ->with('articleFormTemplate', $this->articleFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        $cat = $article->category;

        // show the view and pass the nerd to it
        return View::make('dcms::articles/show')
            ->with('article', $article)
            ->with('category', $cat->title);
    }

    public function getSortOptions($model, $setExtra = 0)
    {
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->id) <= 0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i <= ($maxSortID + $increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }

        return $SortOptions;
    }

    public function getSelectedPages($articleid = null)
    {
        return DB::connection('project')->select('  SELECT article_detail_id, page_id
													FROM articles_language_to_pages
													WHERE article_detail_id IN (SELECT id FROM articles_language WHERE article_id = ?)', [$articleid]);
    }

    public function setPageOptionValues($objselected_pages)
    {
        $pageOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $pageOptionValuesSelected[$obj->page_id] = $obj->page_id;
            }
        }

        return $pageOptionValuesSelected;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $article->startdate = (is_null($article->startdate) ? null : DateTime::createFromFormat('Y-m-d', $article->startdate)->format('m/d/Y'));
        $article->enddate = (is_null($article->enddate) ? null : DateTime::createFromFormat('Y-m-d', $article->enddate)->format('m/d/Y'));

        $objlanguages = $this->getInformation($id);
        $faq = $this->getFaq($id);
        $objselected_pages = $this->getSelectedPages($id);

        return View::make('dcms::articles/form')
            ->with('article', $article)
            ->with('languages', $objlanguages)
            ->with('faq', $faq)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('pageOptionValuesSelected', $this->setPageOptionValues($objselected_pages))
            ->with('sortOptionValues', $this->getSortOptions($objlanguages))
            ->with('articlelanguageFormTemplate', $this->articlelanguageFormTemplate)
            ->with('articleFormTemplate', $this->articleFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * copy the model
     *
     * @param  int $id
     *
     * @return Response
     */
    public function copy($id)
    {
        $newDetail = Detail::find($id)->replicate();
        $Article = new Article();
        $Article->save();
        $newDetail->article_id = $Article->id;
        $newDetail->save();

        return Redirect::to('admin/articles');
    }

    private function validateArticleForm()
    {
        $rules = [
            //	'title' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }

    public function saveFaq(Request $request, $article_id = null)
    {
        if (request()->has('removed_faq')) {
            Faq::destroy(request()->get('removed_faq'));
        }

        if (request()->has('faq')) {
            foreach (request()->get('faq') as $faq_id => $data) {
                if (!empty(trim($data['question']))) {
                    $faq_find = Faq::find($faq_id);
                    $faq = $faq_find ?: new Faq();
                    $faq->article_id = $article_id;
                    $faq->language_id = $data['language_id'];
                    $faq->question = $data['question'];
                    $faq->answer = $data['answer'];
                    $faq->save();
                }
            }
        }
    }

    private function saveArticleProperties(Request $request, $articleid = null)
    {
        $notifymoderator = false;
        // do check if the given id is existing.
        if (!is_null($articleid) && intval($articleid) > 0) {
            $Article = Article::find($articleid);
        }

        if (!isset($Article) || is_null($Article)) {
            $Article = new Article();
            $notifymoderator = true;
        }

        foreach ($request->get('title') as $language_id => $title) {
            if (strlen(trim($request->get('title')[$language_id])) > 0 || strlen(trim($request->get('description')[$language_id])) > 0 || strlen(trim($request->get('body')[$language_id])) > 0) {
                //----------------------------------------
                // once there is a title set
                // we can set the properties to the article
                // and instantly return the article model
                // by default nothing is return so the
                // script may stop.
                //----------------------------------------
                $Article->startdate = (!empty($request->get('startdate')) ? DateTime::createFromFormat('m/d/Y', $request->get('startdate'))->format('Y-m-d') : null);
                $Article->enddate = (!empty($request->get('enddate')) ? DateTime::createFromFormat('m/d/Y', $request->get('enddate'))->format('Y-m-d') : null);

                foreach ($this->article_Columnnames as $column => $inputname) {
                    if ($request->has($inputname)) {
                        $Article->$column = $request->get($inputname);
                    } elseif (array_key_exists($inputname, $this->article_ColumnNamesDefaults)) {
                        $Article->$column = $this->article_ColumnNamesDefaults[$inputname];
                    } else {
                        $Article->$column = null;
                    }
                }

                $Article->save();

                if ($notifymoderator == true) {
                    $this->notifymoderator($Article);
                }

                return $Article;
                break; // we only have to save the global settings once.
            }
        }
        //does not return anything by defuault... (may be false dunno - find out)
    }

    private function saveArticleDetail(Request $request, Article $Article, $givenlanguage_id = null)
    {
        $input = $request->all();
        $Detail = null;

        $pages = [];
        //get all the pages
        if (Schema::hasTable('pages_language')) {
            $pages = DB::connection('project')->table('pages_language')->select('url_slug')->get()->pluck('url_slug')->toArray();
        }

        $plants = [];
        if (Schema::hasTable('plants_language')) {
            $plants = DB::connection('project')->table('plants_language')->select('slug')->get()->pluck('slug')->toArray();
        }
        $plantPageSlugs = array_merge($pages, $plants);

        foreach ($input['title'] as $language_id => $title) {
            if (strlen(trim($input['title'][$language_id])) > 0 || strlen(trim($input['description'][$language_id])) > 0 || strlen(trim($input['body'][$language_id])) > 0) {
                if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id))) {
                    $Detail = null;
                    $newInformation = true;
                    $Detail = Detail::find($input['article_information_id'][$language_id]);

                    if (is_null($Detail) === true) {
                        $Detail = new Detail();
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($Detail->sort_id) && intval($Detail->sort_id) > 0) {
                        $oldSortID = intval($Detail->sort_id);
                    }

                    $Detail->language_id = $language_id;
                    foreach ($this->article_language_Columnnames as $column => $inputname) {
                        if (!in_array($column, ['slug', 'path', 'article_category_id', 'log'])) {
                            if (isset($input[$inputname][$language_id])) {
                                $Detail->$column = $input[$inputname][$language_id];
                            } elseif (array_key_exists($column, $this->information_ColumnNamesDefaults)) {
                                $Detail->$column = $this->information_ColumnNamesDefaults[$column];
                            } else {
                                $Detail->$column = null;
                            }
                        }
                    }

                    if (array_key_exists($language_id, $input[$this->article_language_Columnnames['article_category_id']])) {
                        $Detail->article_category_id = ($input[$this->article_language_Columnnames['article_category_id']][$language_id] == 0 ? null : $input[$this->article_language_Columnnames['article_category_id']][$language_id]);
                    }

                    $url_slug = Str::slug($input[$this->article_language_Columnnames['slug']][$language_id]);

                    //get all existing slugs
                    $existingSlugs = Detail::where('id', '<>', $Detail->id)->where('language_id', $language_id)->get('url_slug')->pluck('url_slug')->toArray();

                    $notAllowedSlugs = array_merge($existingSlugs, $plantPageSlugs);

                    if (in_array($url_slug, $notAllowedSlugs)) {
                        for ($appendix = 1; $appendix <= 15; $appendix++) {
                            if (!in_array($url_slug . '-' . $appendix, $notAllowedSlugs)) {
                                $url_slug = $url_slug . '-' . $appendix;
                                break;
                            }
                        }
                    }
                    $Detail->url_slug = $url_slug;

                    if ($Detail->isDirty() || strlen(trim($input['log'][$language_id])) > 0) {
                        $Detail->log = $Detail->log . "\r\n\r\n" . '[' . Carbon::now() . ' ' . Auth::guard('dcms')->user()->username . ']' . "\r\n" . $input['log'][$language_id];
                    }

                    $Detail->save();
                    $Article->detail()->save($Detail);

                    //----------------------------------------
                    // Detach the $Detail from all pages
                    //----------------------------------------
                    $Detail->pages()->detach();

                    //----------------------------------------
                    // link the article to the selected page
                    // we will take the $Detail->id since
                    // this directly holds the language_id
                    // otherwise we'd be storing it twice
                    //----------------------------------------
                    if (isset($input['page_id']) && isset($input['page_id'][$language_id]) && count($input['page_id'][$language_id]) > 0) {
                        foreach ($input['page_id'][$language_id] as $arrayindex => $page_id) {
                            $Detail->pages()->attach($page_id);
                        }
                    }
                }
            }
        }

        return $Detail;
    }

    public function getSelectedProductsInformation($article_id = null)
    {
        return DB::connection('project')->select('	SELECT products_information_id, articles_language_id
													FROM articles_language_to_products_information
													WHERE articles_language_id IN (SELECT id FROM articles_language WHERE article_id = ?)', [$article_id]);
    }

    public function setProductsInformationOptionValues($objselected_pages)
    {
        $ProductsOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $ProductsOptionValuesSelected[$obj->products_information_id] = $obj->products_information_id;
            }
        }

        return $ProductsOptionValuesSelected;
    }

    public function saveArticleToProductInformation(Request $request, Article $Article)
    {
        $Article->productinformation()->detach();
        if ($request->has('information_group_id') && count($request->get('information_group_id')) > 0) {
            foreach ($request->get('information_group_id') as $i => $information_group_id) {
                $Article->productinformation()->attach($information_group_id, ['information_group_id' => $information_group_id]);
            }
        }
    }

    public function saveArticleToArticle(Request $request, Article $Article)
    {
        $Article->article()->detach();
        if ($request->has('taggedarticle_id') && count($request->get('taggedarticle_id')) > 0) {
            foreach (array_unique($request->get('taggedarticle_id')) as $i => $taggedarticle_id) {
                $Article->article()->attach($taggedarticle_id, ['taggedarticle_id' => $taggedarticle_id]);
            }
        }
    }

    public function saveArticleToPlants(Request $request, Article $Article)
    {
        $plant_ids = $request->has('plant_ids') ? $request->get('plant_ids') : [];
        $Article->plants()->sync($plant_ids);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateArticleForm() === true) {
            $Article = $this->saveArticleProperties($request);

            if (!is_null($Article)) {
                $this->saveArticleDetail($request, $Article);
                $this->saveArticleToProductInformation($request, $Article);
                $this->saveArticleToArticle($request, $Article);
                $this->saveArticleToPlants($request, $Article);
                $this->setCanonicalUrl($request, $Article);
                $this->saveFaq($request, $Article->id);
            }

            // redirect
            Session::flash('message', 'Successfully created article!');

            if ($request->has('SaveAndFormAgain') && $request->get('SaveAndFormAgain') == 'SaveAndFormAgain') {
                return redirect('/admin/articles/' . $Article->id . '/edit');
            }

            return Redirect::to('admin/articles');
        } else {
            return $this->validateArticleForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateArticleForm() === true) {
            $Article = $this->saveArticleProperties($request, $id);

            if (!is_null($Article)) {
                $this->saveArticleDetail($request, $Article);
                $this->saveArticleToProductInformation($request, $Article);
                $this->saveArticleToArticle($request, $Article);
                $this->saveArticleToPlants($request, $Article);
                $this->setCanonicalUrl($request, $Article);
                $this->saveFaq($request, $Article->id);
            }

            // redirect
            Session::flash('message', 'Successfully updated article!');

            if ($request->has('SaveAndFormAgain') && $request->get('SaveAndFormAgain') == 'SaveAndFormAgain') {
                return Redirect::back();
            }

            return Redirect::to('admin/articles');
        } else {
            return $this->validateArticleForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Detail = Detail::find($id);

        $mainArticleID = $Detail->article_id;

        //find the sortid of this product... so all the above can descent by 1
        $updateInformations = Detail::where('language_id', '=', $Detail->language_id)->get(['id', 'sort_id']);
        if (isset($updateInformations) && count($updateInformations) > 0) {
            foreach ($updateInformations as $uInformation) {
                $uInformation->sort_id = intval($uInformation->sort_id) - 1;
                $uInformation->save();
            }//end foreach($updateInformations as $Information)
        }//end 	if (count($updateInformations)>0)

        $Detail->delete();

        if (Detail::where('article_id', '=', $mainArticleID)->count() <= 0) {
            Article::destroy($mainArticleID);
        }

        Session::flash('message', 'Successfully deleted the article!');

        return Redirect::to('admin/articles');
    }

    public function notifymoderator(Article $Article)
    {
        Mail::raw('Dear admin,' . "\r\n" . ' A new article was added by ' . Auth::guard('dcms')->user()->username . ':' . "\r\n" . ' http://www.dcm-info.com/admin/articles/' . $Article->id . '/edit ', function ($message) {
            $message->subject('DCMS - new article');
            $message->from('dcm@dcm-info.com', 'CMS');
            $message->to('phe@groupdc.be');
        });
    }

    public function verifySlug(Article $Article)
    {
        $verifyDoublesArray = [1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => [], 8 => [], 9 => [], 10 => [], 11 => []];

        $ArticlesWhereSlugIsNull = Detail::whereNull('url_slug_new')->get();
        foreach ($ArticlesWhereSlugIsNull as $article) {
            $url_slug_new = $article->url_slug;
            if (in_array($article->url_slug, $verifyDoublesArray[$article->language_id])) {
                for ($appendix = 1; $appendix <= 15; $appendix++) {
                    if (!in_array($article->url_slug . '-' . $appendix, $verifyDoublesArray[$article->language_id])) {
                        $url_slug_new = $article->url_slug . '-' . $appendix;
                        $verifyDoublesArray[$article->language_id][] = $url_slug_new;
                        break;
                    }
                }
            }
        }
        return 'ok';
        //get all the plants
    }

    public function setUrlSlugNew(Request $request)
    {
        $verifyDoublesArray = [1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => [], 8 => [], 9 => [], 10 => [], 11 => []];
        $ArticlesWhereSlugIsNull = Detail::whereNull('url_slug_new')->get();
        foreach ($ArticlesWhereSlugIsNull as $article) {
            $url_slug_new = $article->url_slug;
            if (in_array($article->url_slug, $verifyDoublesArray[$article->language_id])) {
                for ($appendix = 1; $appendix <= 15; $appendix++) {
                    if (!in_array($article->url_slug . '-' . $appendix, $verifyDoublesArray[$article->language_id])) {
                        $url_slug_new = $article->url_slug . '-' . $appendix;
                        $verifyDoublesArray[$article->language_id][] = $url_slug_new;
                        break;
                    }
                }
            }

            $verifyDoublesArray[$article->language_id][] = $url_slug_new;

            $article->url_slug_new = $url_slug_new;
            $article->save();
        }

        // setCanonicalUrl
        $doRunResult = DB::SELECT(DB::raw("
        
        SELECT concat('UPDATE articles_language SET canonical_url = \"', canonical_url , '\" WHERE id = ',T.id ,';') as run
        FROM (
            SELECT * FROM (
                    SELECT
                    articles_categories_language.url_slug,
                    @l := CASE articles_categories_language.url_slug 
                    WHEN 'info' THEN 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/info/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/info/',articles_language.url_slug_new)
                            ELSE concat('info/',articles_language.url_slug_new)
                        END 
                    
                    WHEN 'document' THEN 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/documenten','/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/documenten','/',articles_language.url_slug_new)
                            ELSE concat('documenten','/',articles_language.url_slug_new)
                        END 

                    WHEN 'ausschreibungstexte' THEN 
                
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/ausschreibungstexte','/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/ausschreibungstexte','/',articles_language.url_slug_new)
                            ELSE concat('ausschreibungstexte','/',articles_language.url_slug_new)
                        END 
                        
                    WHEN 'presse' THEN 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('presse/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('presse/',articles_language.url_slug_new)
                            ELSE concat('presse/',articles_language.url_slug_new)
                        END 
                    
                    
                    WHEN 'customer' THEN 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/customer/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/customer/',articles_language.url_slug_new)
                            ELSE concat('customer/',articles_language.url_slug_new)
                        END 
                    
                    
                    WHEN 'video' THEN 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/video/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/video/',articles_language.url_slug_new)
                            ELSE concat('video/',articles_language.url_slug_new)
                        END 
                    
                    WHEN 'advice' THEN 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/tuintips/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/advies/',articles_language.url_slug_new)
                            ELSE concat('tuintip/',articles_language.url_slug_new)
                        END 
                    
                    ELSE 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/',articles_categories_language.url_slug ,'/',articles_language.url_slug_new)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/',articles_categories_language.url_slug ,'/',articles_language.url_slug_new)
                            ELSE concat(articles_categories_language.url_slug ,'/',articles_language.url_slug_new)
                        END 
                    
                    END
                    
                    as canonical_url,
                    
                    length(@l),
                    
                    article_id,
                    articles_language.id
                    
                    FROM articles_language 
                    
                    LEFT JOIN articles_categories_language on articles_language.article_category_id = articles_categories_language.id
                    LEFT JOIN articles_language_to_pages  on articles_language_to_pages.article_detail_id = articles_language.id
                    LEFT JOIN vwpages_language on vwpages_language.id = articles_language_to_pages.page_id
                    WHERE 
                        articles_language.canonical_url is null or articles_language.canonical_url  = '' or article_id = '" . $article->id . "'  
                    ORDER BY 3 DESC,1
                        ) as TT 

        GROUP BY TT.id ) as T"));

        foreach ($doRunResult as $r) {
            if (isset($r->run)) {
                DB::SELECT(DB::RAW($r->run));
            }
        }

        return 'ok';
    }

    public function setCanonicalUrl(Request $request, $article)
    {
        // DB::SELECT(DB::RAW("LOCK TABLES articles_language WRITE, articles_categories_language WRITE, vwpages_language WRITE, articles_language_to_pages WRITE"));
        $doRunResult = DB::SELECT(DB::raw("
        
        SELECT concat('UPDATE articles_language SET canonical_url = \"', canonical_url , '\" WHERE id = ',T.id ,';') as run
        FROM (
            SELECT * FROM (
                    SELECT
                    articles_categories_language.url_slug,
                    @l := CASE articles_categories_language.url_slug 
                    
                    WHEN 'info' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/info/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/info/',articles_language.url_slug)
                            ELSE concat('info/',articles_language.url_slug)
                        END 
                    
                    WHEN 'document' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/documenten','/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/documenten','/',articles_language.url_slug)
                            ELSE concat('documenten','/',articles_language.url_slug)
                        END 

                    WHEN 'ausschreibungstexte' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/ausschreibungstexte','/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/ausschreibungstexte','/',articles_language.url_slug)
                            ELSE concat('ausschreibungstexte','/',articles_language.url_slug)
                        END 
                        
                    WHEN 'presse' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('presse/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('presse/',articles_language.url_slug)
                            ELSE concat('presse/',articles_language.url_slug)
                        END 
                    
                    WHEN 'customer' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/customer/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/customer/',articles_language.url_slug)
                            ELSE concat('customer/',articles_language.url_slug)
                        END 

                    WHEN 'video' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/video/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/video/',articles_language.url_slug)
                            ELSE concat('video/',articles_language.url_slug)
                        END 

                    WHEN 'advice' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/tuintips/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/advies/',articles_language.url_slug)
                            ELSE concat('tuintip/',articles_language.url_slug)
                        END 

                   WHEN 'happy-customer' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/happy-customer-customer-case/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/happy-customer-customer-case/',articles_language.url_slug)
                            ELSE concat('happy-customer-customer-case/',articles_language.url_slug)
                        END     

                    WHEN 'customer-case' THEN 
                            CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/happy-customer-customer-case/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/happy-customer-customer-case/',articles_language.url_slug)
                            ELSE concat('happy-customer-customer-case/',articles_language.url_slug)
                        END 

                    WHEN 'innovation' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/innovations/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/innovations/',articles_language.url_slug)
                            ELSE concat('innovations/',articles_language.url_slug)
                        END 

                    WHEN 'happy-customer-customer-case' THEN 
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/happy-customer-customer-case/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/happy-customer-customer-case/',articles_language.url_slug)
                            ELSE concat('happy-customer-customer-case/',articles_language.url_slug)
                        END 
                ELSE 
                    
                        CASE WHEN INSTR(vwpages_language.division , \"hobby\") > 0 
                            THEN concat('hobby/',articles_categories_language.url_slug ,'/',articles_language.url_slug)
                            WHEN INSTR(vwpages_language.division , \"pro\") > 0 
                            THEN concat('pro/',articles_categories_language.url_slug ,'/',articles_language.url_slug)
                            ELSE concat(articles_categories_language.url_slug ,'/',articles_language.url_slug)
                        END 
                    
                END
                    
                    as canonical_url,
                    
                    length(@l),
                    
                    article_id,
                    articles_language.id
                    
                    FROM articles_language 
                    
                    LEFT JOIN articles_categories_language on articles_language.article_category_id = articles_categories_language.id
                    LEFT JOIN articles_language_to_pages  on articles_language_to_pages.article_detail_id = articles_language.id
                    LEFT JOIN vwpages_language on vwpages_language.id = articles_language_to_pages.page_id
                    WHERE 
                        articles_language.canonical_url is null or articles_language.canonical_url  = '' or article_id = '" . $article->id . "'  
                    ORDER BY 3 DESC,1
                        ) as TT 

        GROUP BY TT.id ) as T"));

        foreach ($doRunResult as $r) {
            if (isset($r->run)) {
                DB::SELECT(DB::RAW($r->run));
            }
        }

        //update the canonical url via queu
        ProcessUpdateArticle::dispatch();
    }
}
