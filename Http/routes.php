<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::group(['middleware' => 'auth:dcms'], function () {
            //ARTICLES
            Route::group(['prefix' => 'articles', 'as' => 'articles.'], function () {
                Route::get('{id}/copy', ['as' => 'copy', 'uses' => 'ArticleController@copy']);

                //CATEGORIES
                Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
                    Route::get('{id}/copy', ['as' => '{id}.copy', 'uses' => 'CategoryController@copy']);
                    Route::any('api/table', ['as' => 'api.table', 'uses' => 'CategoryController@getDatatable']);
                });
                Route::resource('categories', 'CategoryController');

                //API
                Route::group(['prefix' => 'api', 'as' => 'api.'], function () {
                    Route::any('table', ['as' => 'table', 'uses' => 'ArticleController@getDatatable']);
                    Route::any('products/table/{article_id?}', ['as' => 'products.table', 'uses' => 'ArticleController@getProductsDatatable']);
                    Route::any('relativearticles/table/{article_id?}', ['as' => 'relativearticles.table', 'uses' => 'ArticleController@getRelativeArticlesDatatable']);
                    Route::any('relation/table/{product_id?}', ['as' => 'relation.table', 'uses' => 'ArticleController@getRelationDatatable']);
                    Route::any('plants/table/{article_id?}', ['as' => 'plants.table', 'uses' => 'ArticleController@getPlantsDatatable']);
                    Route::any('faqrow', ['as' => 'faqrow', 'uses' => 'ArticleController@getFaqrow']);
                });
            });
            Route::resource('articles', 'ArticleController');
        });
    });
});
